#ifndef __COMMON__
#define __COMMON__

#define assert(b) assert0( (b), __FUNCTION__, __LINE__, __FILE__ )
#define MALLOC_MEMORY_SIZE 131072 /* 2 ^ 17 */

unsigned char malloc_memoy[MALLOC_MEMORY_SIZE];

void assert0(int b, const char * fct_name, int line, const char * file);
void test_assert();
void * my_malloc(int bytes);
void my_free(void * arg);


#endif