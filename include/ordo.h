#ifndef __ORDO_H__
#define __ORDO_H__

#define enable_it() __asm volatile("sti")
#define disable_it() __asm volatile("cli")

#define CTX_INIT 1
#define CTX_RUN 2
#define CTX_FINISH 3
#define CTX_BLOCK 4

typedef void (func_t) (void *);


typedef struct semaphore_s {
    int value;
} semaphore;

struct ctx_s {

    void * esp;
    void * ebp;
    void * args;
    func_t * f;
    int state;
    semaphore * sem_block;
    char * stack;
    struct ctx_s * next;
};



void switch_to_ctx(struct ctx_s *ctx);
int init_ctx(struct ctx_s *ctx, int stack_size, func_t f, void *args);
void start_shed();
void debug();
void my_yield();
void create_ctx(int stack_size, func_t f, void *args);
void init_sem(semaphore *sem, int val);
void init_mutex(semaphore *sem);
void sem_up(semaphore * sem);
void sem_down(semaphore * sem);
#endif