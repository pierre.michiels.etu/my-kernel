#ifndef __KEYBOARD_H__
#define __KEYBOARD_H__

#define NORMAL 0
#define CAPS 1
#define CAPS_LOCK 2

#define DATA_PORT 0x60
#define STATE_PORT 0x64

#define CAPS_LOCK_CODE 0x3A
#define LEFT_SHIFT_CODE 0x2A
#define RIGHT_SHIFT_LOCK 0x36
#define BACKSPACE 0x08

char keyboard_map(unsigned char key_code);
void write_character(char car);
void print_keyboard();
char get_keyboard_input();

void print_ascii_read_state();
char getc();
void init_keyboard();
#endif