#ifndef __DISPLAY_H__
#define __DISPLAY_H__

void print_helloWorld();      /* print the famous helloWorld on the screen */
void clear_screen();				/* clear screen */
void set_pos_cursor(int x, int y);
void get_current_cursor_pos(int *x, int *y);
void putc(char aChar);				/* print a single char on screen */
void puts(const char *aString);			/* print a string on the screen */
void puthex(int aNumber);			/* print an Hex number on screen */


#endif