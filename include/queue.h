#ifndef __QUEUE_H__
#define __QUEUE_H__

typedef struct keyboard_elem_s keyboard_elem;

struct keyboard_elem_s{
    char kb_input;
    keyboard_elem *next;
};

typedef struct keyboard_fifo_s keyboard_fifo;

struct keyboard_fifo_s {
    keyboard_elem *first;
    keyboard_elem *last;
};

void kb_fifo_push(keyboard_fifo *fifo, char ipt);
char kb_fifo_pop(keyboard_fifo *fifo);
void init_kb_fifo(keyboard_fifo *fifo);

#endif