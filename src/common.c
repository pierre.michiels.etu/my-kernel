#include "display.h"
#include "common.h"
#include <stddef.h>

void assert0 (int b, const char * fct_name, int line, const char * file){

  if(!b){
    puts("assert fail ");
    puts(fct_name);
    puts(" on file ");
    puts(file);
    puts(" line 0x");
    puthex(line);
    puts("\n");
  }
}

unsigned int start_malloc_idx = 0;


void * my_malloc(int bytes){

  unsigned short next_start_malloc_ptr = start_malloc_idx + bytes + 1; // 1 case mémoire pour y mettre NULL et séparer les différentes allocation mémoire */

  if(next_start_malloc_ptr >= MALLOC_MEMORY_SIZE)
    return NULL;

  unsigned char * ptr = &malloc_memoy[start_malloc_idx];
  malloc_memoy[next_start_malloc_ptr-1] = 0;

  start_malloc_idx = next_start_malloc_ptr;


  return (void *) ptr;  
}