#include "ordo.h"
#include "common.h"
#include "idt.h"
#include "ioport.h"
#include "display.h"
#include <stddef.h>

struct ctx_s * current_ctx = NULL;
struct ctx_s * first_ctx;


// on crée la structure contexte avec les champs donnée en paramètre
void create_ctx(int stack_size, func_t f, void *args){
    struct ctx_s * ctx;

    ctx = (struct ctx_s *) my_malloc(sizeof(struct ctx_s));

    init_ctx(ctx, stack_size, f, args);
    
    if(current_ctx == NULL){
        current_ctx = ctx;
        first_ctx = ctx;
    } else {
        ctx->next = first_ctx;
    }

    current_ctx->next = ctx;
    current_ctx = ctx;
    
}

int init_ctx(struct ctx_s *ctx, int stack_size, func_t f, void *args){

    char * stack;

    stack = (char *) my_malloc(stack_size);
    
    if(stack == NULL)
        return -1;

    ctx->ebp = &stack[stack_size-sizeof(void*)];
    ctx->esp = &stack[stack_size-sizeof(void*)];
    ctx->f = f;
    ctx->args = args;
    ctx->state = CTX_INIT;
    ctx->sem_block = NULL;
    ctx->stack = stack;

    return 0;

}


void start_ctx(){

    // on ré-autorise les it
    __asm volatile("sti");
    _outb(0x20, 0x20);

    current_ctx->state = CTX_RUN;
    current_ctx->f(current_ctx->args);
    current_ctx->state = CTX_FINISH;
    //free(current_ctx->stack);
}

void switch_to_ctx(struct ctx_s *ctx){

    __asm volatile("sti");  // on enlève les IT pour éviter de couper le switch_to_tcx

    assert(ctx != NULL);

    
    if(current_ctx != NULL){
        asm("mov %%esp, %0"
        :"=r"(current_ctx->esp));

        asm("mov %%ebp, %0"
        :"=r"(current_ctx->ebp));
    }
    

    current_ctx = ctx;

    asm("mov %0, %%esp"::"r"(current_ctx->esp));

    asm("mov %0, %%ebp"::"r"(current_ctx->ebp));

    if(current_ctx->state == CTX_INIT){
        start_ctx();
    }

    __asm volatile("cli"); // on remets les it 

}

// appellé a chaque IT Timer
void my_yield(){
    struct ctx_s * ctx = current_ctx->next;

    // on vérifie si le prochain contexte n'est pas bloqué par un sémaphore
    // si c'est le cas on en choisis un autre
    // si on retombe sur le contexte courant, on est revenue au début de la boucle et on relance le contexte courant
    while(ctx != current_ctx){
        if(ctx->state == CTX_RUN || ctx->state == CTX_INIT)
            break;
        
        ctx = ctx->next;
    }

    switch_to_ctx(ctx);  
}


void start_shed(){

    idt_setup_handler(0, my_yield);

    switch_to_ctx(current_ctx);
    
}

void init_sem(semaphore * sem, int val){
    sem->value = val;
}

void init_mutex(semaphore * sem){
    sem->value = 1;
}

void sem_up(semaphore * sem){
    disable_it();

    // si un context veut prendre le sem alorss qu'il n'est pas disponible
    // on le bloque et on relance l'ordo
    if(sem->value <= 0){
        current_ctx->state = CTX_BLOCK;
        current_ctx->sem_block = sem;
        my_yield();
    }
    sem->value--;
    enable_it();
}

void sem_down(semaphore * sem){
    struct ctx_s * ctx = current_ctx;

    disable_it();

    // si un context attends le sem
    // on cherche parmuis les contexte lesquel sont bloqué par le sémaphore
    // on fois trouver, on le remet en RUN et on relance l'ordo
    if(sem->value <= 0){

        do {
            
            if(ctx->state == CTX_BLOCK && ctx->sem_block == sem){
                ctx->state = CTX_RUN;
                current_ctx->next = ctx;
                break;
            }
            ctx = ctx->next;
        } while (ctx != current_ctx);
        my_yield();
    }
    sem->value++;
    enable_it();
}