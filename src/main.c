
#include "keyboard.h"
#include "display.h"
#include "gdt.h"
#include "idt.h"
#include "ioport.h"
#include "common.h"
#include "ordo.h"
#include "queue.h"
#include <stddef.h>

int cpt=0;

void f_ping(void *args);
void f_pong(void *args);
void f_pang(void *args);
void f_keyboard(void *args);
void f_counter(void *args);
void keyboard_it_queue(int_regs_t *r);
void print_queue(int_regs_t *r);
void f_getc(void *args);
void f_getc_b(void *args);

semaphore screen_sem;
keyboard_fifo kb_fifo;
int cpt;


void empty_irq(int_regs_t *r) {

}

/* multiboot entry-point with datastructure as arg. */
void main(unsigned int * mboot_info)
{   
  cpt = 0;
  
  /* clear the screen */
  clear_screen();
  puts("Early boot.\n"); 
  puts("\t-> Setting up the GDT... ");
  gdt_init_default();
  puts("done\n");

  puts("\t-> Setting up the IDT... ");
  setup_idt();
  puts("OK\n");

  puts("\n\n");


  /*** décommenté cette partie si on utilise les IT clavier ***/

  // puts("\t-> Setting up the keyboard... ");
  // init_keyboard();
  // puts("OK\n");

  __asm volatile("sti");

  /* minimal setup done ! */
  clear_screen();  

  /*** test sched ***/

  create_ctx(16384, f_ping, NULL);
  create_ctx(16384, f_pong, NULL);
  create_ctx(16384, f_pang, NULL);
  start_shed();

  /*** test shed + sémaphore***/

  // init_sem(&screen_sem, 1);
  // create_ctx(16384, f_keyboard, NULL);
  // create_ctx(16384, f_counter, NULL);
  // start_shed();

  /*** test interruption ***/

  // init_kb_fifo(&kb_fifo);

  // idt_setup_handler(1, keyboard_it_queue);
  // idt_setup_handler(0, print_queue);

  /*** test getc ***/
  
  // init_sem(&screen_sem, 1);
  // create_ctx(16384, f_getc, NULL);
  // create_ctx(16384, f_getc_b, NULL);
  // start_shed();

  while(1){
    //print_asciiCode();    
  }
}

void keyboard_it_queue(int_regs_t *r){
  // print_keyboard();
  char kb_car;

  kb_car = get_keyboard_input();

  // évite de déplacer le curseur quand c'est un caractère non valide (caps lock, etc.)
  // write_character(kb_car);
  if(kb_car != 0){
    kb_fifo_push(&kb_fifo, kb_car);
  }

}

void print_queue(int_regs_t *r){

  cpt++;

  if(cpt <= 10)
    return;
  
  cpt = 0;
  char ipt;

  while( (ipt=kb_fifo_pop(&kb_fifo)) != 0)
    write_character(ipt);
}

void f_getc(void *args){

  char k;

  while(1){

    k = getc();

    sem_up(&screen_sem);
    putc('*');

    write_character(k);
    
    sem_down(&screen_sem);

  }
}


void f_getc_b(void *args){

  char k;

  while(1){

    k = getc();

    sem_up(&screen_sem);
    putc('+');

    write_character(k);
    
    sem_down(&screen_sem);

  }
}

void f_keyboard(void *args){


  while(1){


    sem_up(&screen_sem);

    print_ascii_read_state();
    
    sem_down(&screen_sem);

  }
}

void f_counter(void *args){
  int last_x, last_y;

  while(1){
  
    sem_up(&screen_sem);

    cpt++;
    get_current_cursor_pos(&last_x, &last_y);
    set_pos_cursor(70, 0);
    puthex(cpt);
    set_pos_cursor(last_x, last_y);

    sem_down(&screen_sem);
  }
}

void test_assert(){
  assert(1==2);
}

void f_ping(void *args){    
    while(1){
      putc('a');
    }
}

void f_pong(void *args){

    while(1){
      putc('1');
    }
}

void f_pang(void *args){

    while(1){
      putc('+');
    }
}
