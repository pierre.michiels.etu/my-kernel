#include "keyboard.h"
#include "display.h"
#include "ioport.h"
#include "ordo.h"
#include "idt.h"
#include <stdlib.h>

unsigned char caps_state;
semaphore keyboard_sem;

char key;

void get_keyboard_input_it(int_regs_t *r);

// initialise les variables interne au bon fonctionnemebnt du clavier
void init_keyboard(){

  init_mutex(&keyboard_sem);
  caps_state = NORMAL;
  key = 0;
  idt_setup_handler(1, get_keyboard_input_it);

}

char keyboard_map(unsigned char key_code){
    char ret = 0;
    char normal_car[60] = "\x00\x00&\x00\"'(-\x00_\x00\x00)=\x8\tazertyuiop^$\n\x00qsdfghjklm\x00\x00\x00\x00wxcvbn,;:!\x00\x00\x00 \0";
    char caps_car[60] =   "\0\0001234567890\0+\x8\tAZERTYUIOP\0\0\n\0QSDFGHJKLM\0*\0\0WXCVBN?./\0\0\0\0 \0";
    unsigned char len = 60;

    // 0x2A ou 0x36 correspondent au shift enfoncé gauche ou droit du clavier
    if(key_code == LEFT_SHIFT_CODE || key_code == RIGHT_SHIFT_LOCK){
      caps_state = CAPS;
      return 0;
    }

    // 0x3A corresponds à la touche shift lock
    if(key_code == CAPS_LOCK_CODE){
      if(caps_state == CAPS_LOCK)
        caps_state = NORMAL;
      else
        caps_state = CAPS_LOCK;

      return 0;  
    }
      
    // keycode va de 0 à 59 pour les touches lettres/symboles
    if(key_code < len){
      switch (caps_state)
      {
      case NORMAL:
        ret = normal_car[key_code];
        break;
      
      case CAPS:
        ret = caps_car[key_code];
        caps_state = NORMAL;
        break;

      case CAPS_LOCK:
        ret = caps_car[key_code];
        break;
      // on renvoi 0 pour toute autre touche appuyée
      default:
        ret = 0;
      }
    }

    return ret;
}

void write_character(char car){
    if(car == 0){
      return;
    }

    if(car == BACKSPACE){
      putc(BACKSPACE);
      putc('\0');
      putc(BACKSPACE);
      return;
    }

    putc(car);

}

void print_keyboard(){

  write_character(get_keyboard_input());
    
}

char get_keyboard_input(){
  unsigned char key_press, ascii;

  key_press = _inb(DATA_PORT);

  return keyboard_map(key_press);
}

// depreceted
void print_ascii_read_state(){

    unsigned char state_code;
    // lecture sur le port 0x64
    state_code = _inb(STATE_PORT);

    // 0x01 corresponds au chiffre hexadécimal '1'
    // on teste ici si le bit 0 (output buffer status, hexa '0x01' pour y accèder) de state_code est à '0' (= vide) ou a '1' (= rempli)
    // si = '1' alors on lis le code présent dans le le data port (= '0x60')

    if( (state_code & 0x01) == 1){
      print_keyboard();
    }
    
}

void get_keyboard_input_it(int_regs_t *r){
  key = keyboard_map(_inb(DATA_PORT));
}

char getc(){
  char ipt_car;

  sem_up(&keyboard_sem);

  while( key == 0);
  ipt_car = key;
  key = 0;
  
  sem_down(&keyboard_sem);

  

  return ipt_car;
}
