#include "queue.h"
#include "common.h"
#include <stddef.h>

void init_kb_fifo(keyboard_fifo *fifo){
    fifo->first = NULL;
}

void kb_fifo_push(keyboard_fifo *fifo, char ipt){

    if (fifo == NULL)
        return;

    keyboard_elem *elem;
    elem = (keyboard_elem *) my_malloc(sizeof(keyboard_elem));
    elem->kb_input = ipt;
    elem->next = NULL;

    if(fifo->first == NULL){
        fifo->first = elem;
        fifo->last = elem;
    } else {
        fifo->last->next = elem;
        fifo->last = elem;
    }
        

    
}

char kb_fifo_pop(keyboard_fifo *fifo){

    if(fifo == NULL)
        return 0;

    if(fifo->first == NULL)
        return 0;

    char output = fifo->first->kb_input;
    fifo->first = fifo->first->next;
    return output;
}